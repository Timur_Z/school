package stuff;

import student.Student;

import java.util.List;

public interface Teachable {

    void teach(Student  student);
    void teach (List<Student> studentList);


}

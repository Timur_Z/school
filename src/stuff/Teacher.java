package stuff;

import student.Student;

import java.util.List;

public class Teacher extends Person  implements Teachable,CheckHomework{
    public Teacher(String name, int age, Sex sex) {
        super(name, age, sex);
    }

    @Override
    public void check(Student student) {
        if (student.getAverageMark() == 10){
            System.out.println(student.getName() + " has already maximum mark");
        }
        student.setAverageMark(student.getAverageMark()+1);
    }

    @Override
    public void teach(Student student) {

        student.setIQ(student.getIQ()+1);
        System.out.println(student.getName() + " current IQ=" + student.getIQ());

    }

    @Override
    public void teach(List<Student> studentList) {
        for (Student s : studentList){
            s.setIQ(s.getIQ()+1);
            System.out.println(s.getName() + " current IQ=" + s.getIQ());
        }
    }
}

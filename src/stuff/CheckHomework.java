package stuff;

import student.Student;

public interface CheckHomework {

    public void check(Student student);
}

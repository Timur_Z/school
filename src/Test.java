import student.Student;
import student.comparators.*;


import java.util.ArrayList;
import java.util.List;

import static stuff.Sex.*;

public class Test {
    public static void main(String[] args) {
        List<Student> studentList = new ArrayList<>();
        Student one = new Student("Petr",10,Male);
        one.setAverageMark(5);
        Student two = new Student("Tim",9,Male);
        two.setAverageMark(8);
        Student three = new Student("John",5,Male);
        three.setAverageMark(10);
        Student four = new Student("Nika",7,Female);
        four.setAverageMark(4);
        Student five = new Student("Vika",6,Female);
        five.setAverageMark(5);

        studentList.add(one);
        studentList.add(two);
        studentList.add(three);
        studentList.add(four);
        studentList.add(five);

        System.out.println("Сортируем учащихся по оценкам");
        studentList.sort(new CompareByMark<>());
        for (Student s : studentList){
            System.out.println(s);
        }

        System.out.println("Сортируем учащихся по именам");
        studentList.sort(new CompareByName<>());
        for (Student s : studentList){
            System.out.println(s);
        }




    }
}

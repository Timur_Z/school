package student.comparators;

import student.Student;

import java.util.Comparator;

public class CompareByName<T> implements Comparator<Student> {
    @Override
    public int compare(Student student, Student t1) {
        return student.getName().compareTo(t1.getName());
    }
}

package student.comparators;

import student.Student;

import java.util.Comparator;

public class CompareByMark<T>  implements Comparator<Student> {
    @Override
    public int compare(Student student, Student t1) {
        return Integer.compare(student.getAverageMark(),t1.getAverageMark());
    }
}

package student;

import stuff.Sex;

public class Student extends Children implements doHomework, doPlaying {

    public void setIQ(int IQ) {
        this.IQ = IQ;
    }

    private int IQ = 50;

    public Student(String name, int age, Sex sex) {
        super(name, age, sex);
    }

    private int averageMark;


    public int getAverageMark() {
        return averageMark;
    }

    public void setAverageMark(int averageMark) {
        if (averageMark > 0 && averageMark <= 10){
        this.averageMark = averageMark;
        } else {
            System.out.println("Input correct mark from 1 to 10");
        }
    }


    @Override
    public String toString() {
        return "Student{" +
                "averageMark=" + averageMark +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", sex=" + sex +
                '}';
    }

    @Override
    public void studying() {
        if(IQ>149){
            System.out.println("Student " + this.getName() + "don't need to study more. IQ level = " + IQ);
            return;
        }
        IQ++;
    }

    @Override
    public final void playGame() {
        if (IQ < 35 || averageMark < 2){
            System.out.println(getName() + " can't play right now. Need to study more");
            return;
        }
        IQ--;
        averageMark --;
    }

    public int getIQ() {
        return IQ;
    }
}
